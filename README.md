FunCaptcha for Drupal
=================

Stop spam with a free, fun, fast CAPTCHA game
Spammers abuse your site, but users hate typing out twisty letters. This significantly reduces user conversions, and it’s just not OK any more. FunCaptcha presents a mini-game that blocks the bots while giving your users a few moments of fun. It’s a real security solution hardened by experts and automatically updated to provide the best protection.

Users complete these little games faster than other CAPTCHAs, with no frustrating failures and no typing. FunCaptcha works on all browsers and mobile devices. It’s easy to implement, so join thousands of other sites and try it!

You can get started and view a demo at our [website](https://www.funcaptcha.com).

## Registration
You’ll need to register on our [website](https://www.funcaptcha.com) and add your domains.  Once you have registered, you can add your website URL which will generate a private and public key. These keys are used in the plugin to authenticate your website with our servers.

## Setup Requirements

Our Drupal plugin currently supports 7.x.

FunCaptcha for Drupal currently requires the CAPTCHA module. Please install the CAPTCHA module if you do not have this installed. https://www.drupal.org/project/captcha

## Setup

Upload the FunCaptcha plugin files to your website, there is three ways to do this in Drupal:

A. Upload from URL

1. Login to the admin area in your Drupal  site, then select Modules from the top menu.
2. Select 'Install new module'.
3. Put in "https://github.com/FunCaptcha/funcaptcha-drupal/archive/master.zip" into the 'Install from a URL' field and click 'Install'.

B. Upload through the admin interface:

1. Login to the admin area in your Drupal site, then select Modules from the top menu.
2. Select 'Install new module'.
3. Click choose folder to upload the FunCaptcha Drupal plugin zip, then click 'Install'.

C. FTP / Manual Upload:

1. Unzip the FunCaptcha folder
2. Upload/FTP the funcaptcha folder to your core modules folder in your Drupal website. This can be found under the modules folder.
3. Login to the admin area in your Drupal site.

Once uploaded, follow these steps:

1. FunCaptcha will now appear under Spam Control. Tick "ENABLED" and click "Save configuration".
2. Click on Configure in the Operations section or select Configuration from the top Menu.
3. To complete the setup public and private keys need to be added. These can be found by: Returning to the FunCaptcha Dashboard - www.funcaptcha.com/dashboard. Scrolling down to the "Your Site Settings", under "Manage" click the "Settings" icon.
4. Copy the Public Key from the FunCaptcha Dashboard and paste to the FunCaptcha Public Key in the Drupal Configuration Dashboard.
5. Copy the Private Key from the FunCaptcha Dashboard and paste to the FunCaptcha Private Key in the Drupal Configuration Dashboard.
6. Click "Save configuration".
7. In the Drupal Dashboard, select the "CAPTCHA" tab top right hand side of screen.
8. In the "Default challenge type" dropdown select "FunCaptcha".
9. Click "Save configuration"
10. Return to the front end of your website and view the form that now contains FunCatpcha.

Everything should now be up and working. Please [contact us](https://www.funcaptcha.com/contact-us/) if you have any issues or questions.