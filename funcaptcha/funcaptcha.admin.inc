<?php

function funcaptcha_admin_settings() {
  $form = array();
  $form['funcaptcha_theme'] = array(
    '#type' => 'select',
    '#title' => t('FunCaptcha Theme '),
    '#description' => t('This will change the appearance of FunCaptcha') ,
    '#options' => array(
                    0 => 'Standard',
                    2 => 'vBulletin Blue',
                    3 => 'White',
                    4 => 'Black',
                    5 => 'Automotive'
                ),
    '#default_value' => variable_get('funcaptcha_theme', '0'),
  );
  
  $form['funcaptcha_security'] = array(
    '#type' => 'radios',
    '#title' => t('FunCaptcha Security Level'),
    '#description' => t('If you choose Automatic, security starts at the lowest level, and rises and falls automatically, adjusted by FunCaptcha\'s monitoring system. The Enhanced level has more challenges to solve, but is very hard for spammer programs to get past'),
    '#options' => array(
                    0 => 'Automatic',
                    20 => 'Always Enhanced'
                ),
    '#default_value' => variable_get('funcaptcha_security', 0),
  );

  
  $form['funcaptcha_js'] = array(
    '#type' => 'radios',
    '#title' => t('Javascript Fallback'),
    '#description' => t('If the user does not have Javascript enabled, display a fallback CAPTCHA? (Most bots have Javascript disabled, we recommend you leave this disabled)'),
    '#options' => array(
                    'Disabled',
                    'Enabled'
                ),
    '#default_value' => variable_get('funcaptcha_js', 0),
  );
  
  $form['funcaptcha_public'] = array(
    '#type' => 'textfield',
    '#title' => t('FunCaptcha Public Key '),
    '#default_value' => variable_get('funcaptcha_public', ''),
    '#description' => t('Public Key (To get the value for "Public key" field please register  your site at <a href="https://www.funcaptcha.com" target="_blank">www.funcaptcha.com</a>)'),
    '#required' => TRUE,
  );
  
  $form['funcaptcha_private'] = array(
    '#type' => 'textfield',
    '#title' => t('FunCaptcha Private Key '),
    '#default_value' => variable_get('funcaptcha_private', ''),
    '#description' => t('Private Key (To get the value for "Private key" field please register  your site at <a href="https://www.funcaptcha.com" target="_blank">www.funcaptcha.com</a>)'),
    '#required' => TRUE,
  );
  
  $form['funcaptcha_proxy'] = array(
    '#type' => 'textfield',
    '#title' => t('Optional - Proxy Server '),
    '#default_value' => variable_get('funcaptcha_proxy', ''),
    '#description' => t('This field is optional - Proxy server (including port, eg: 111.11.11.111:8080)'),
    '#required' => FALSE,
  );
  
    $funcaptcha_private = variable_get('funcaptcha_private', '');
    $funcaptcha_public = variable_get('funcaptcha_public', '');
    if ( ( $funcaptcha_private = '' ) || ( $funcaptcha_public == '' ) ) {
      $form['funcaptcha_web'] = array(
        '#type' => 'item',
        '#markup' => ''
      );
    }
  
  
  
  
  
  
  return system_settings_form($form);
}

