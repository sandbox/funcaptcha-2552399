<?php

/**
 * @file
 * Code required for FunCaptcha
 */

/**
 * Let the user know that the FunCaptcha Private key and JS code not set
 */
function _funcaptcha_check_vars() {
  global $_funcaptcha_calledalready;
  if (!$_funcaptcha_calledalready) {
    $_funcaptcha_calledalready = TRUE;
    $funcaptcha_private = variable_get('funcaptcha_private', '');
    $funcaptcha_public = variable_get('funcaptcha_public', '');
    if ( ( $funcaptcha_private = '' ) || ( $funcaptcha_public == '' ) ) {
      drupal_set_message(t('FunCaptcha is not configured yet'), 'error');
    }
  }
}

